import urllib.request
import re


def test_setup():
    url = 'https://the-internet.herokuapp.com/context_menu'
    global contents
    contents = urllib.request.urlopen(url).read().decode('utf-8')


def test_right_click_text():
    text = "Right-click in the box below to see one called 'the-internet'"
    assert search_text(text) is not None, '{} has not found on the website'.format(text)


def search_text(text: str):
    global contents
    return re.search(">.*{}.*<".format(text), contents)
